
import sqlalchemy as sa
import sqlalchemy.orm as so
from app import create_app, db
from app.models import User, Post, Message, Notification, Comment, Like

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return dict(sa=sa, so=so, db=db, User=User, Post=Post, Message=Message, Notification=Notification,
                Comment=Comment, Like=Like)


if __name__ == "__main__":
    app.run(debug=True)
