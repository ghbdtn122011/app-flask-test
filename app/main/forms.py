
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Length, Email
import sqlalchemy as sa
from flask_babel import _, lazy_gettext as _l
from app import db
from app.models import User


class EditProfileForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired(), Length(min=1, max=20)])
    about_me = TextAreaField(_l('About me'), validators=[Length(min=0, max=140)])
    email = StringField(_l('Email'), validators=[DataRequired(), Email()])
    # picture = FileField(_l('Image png, jpg'), validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField(_l('Submit'))

    def __init__(self, original_username, original_email, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.original_username = original_username
        self.original_email = original_email

    def validate_username(self, username):
        if username.data != self.original_username:
            user = db.session.scalar(sa.select(User).where(
                User.username == self.username.data))
            if user is not None:
                raise ValidationError(_l('Please use a different username'))

    def validate_email(self, email):
        if email.data != self.original_email:
            user = db.session.scalar(sa.select(User).where(
                User.email == self.email.data))
            if user is not None:
                raise ValidationError(_l('Please use a different email'))


class EmptyForm(FlaskForm):
    submit = SubmitField(_l('Submit'))


class PostForm(FlaskForm):
    title = TextAreaField(_l('Write title'), validators=[
        DataRequired(), Length(min=1, max=40)])
    text = TextAreaField(_l('Say something'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))


class MessageForm(FlaskForm):
    message = TextAreaField(_l('Message'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))


class AddCommentForm(FlaskForm):
    comment_body = TextAreaField(_l('Add your comment'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))


class UpdateCommentForm(FlaskForm):
    comment_body = TextAreaField(_l('Update your comment'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))






