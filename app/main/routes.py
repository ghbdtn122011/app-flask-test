
from datetime import datetime, timezone
from flask import render_template, url_for, request, redirect, flash, g, \
    current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
import sqlalchemy as sa
from langdetect import detect, LangDetectException
from app import db
from app.main.forms import EditProfileForm, EmptyForm, PostForm, \
    MessageForm, AddCommentForm, UpdateCommentForm
from app.models import User, Post, Message, Notification, Comment, Like
from app.translate import translate
from app.main import bp


@bp.before_request
def before_request():
    g.locale = str(get_locale())
    if current_user.is_authenticated:
        current_user.last_seen = datetime.now(timezone.utc)
        db.session.commit()


@bp.route("/index", methods=['GET', 'POST'])
@bp.route("/", methods=['GET', 'POST'])
@login_required
def index():
    page = request.args.get('page', 1, type=int)
    pagination = Post.query.order_by(Post.date.desc()).paginate(
        page=page, per_page=current_app.config['POSTS_PER_PAGE'],
        error_out=False)
    posts = pagination.items
    return render_template('index.html', posts=posts,
                           pagination=pagination)


@bp.route("/posts")
def posts():
    page = request.args.get('page', 1, type=int)
    pagination = db.paginate(current_user.following_posts(), page=page, per_page=current_app.config[
        'POSTS_PER_PAGE'], error_out=False)
    posts = pagination.items
    return render_template('posts.html', posts=posts,
                           pagination=pagination)


@bp.route('/user/<username>')
@login_required
def user(username):
    user = db.first_or_404(sa.select(User).where(User.username == username))
    page = request.args.get('page', 1, type=int)
    query = user.posts.select().order_by(Post.date.desc())
    pagination = db.paginate(
        query, page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False
    )
    posts = pagination.items
    form = EmptyForm()
    return render_template(
        'user.html', user=user, posts=posts,
        pagination=pagination, form=form
    )


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username, current_user.email)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved'), 'success')
        return redirect(url_for('main.user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
        form.email.data = current_user.email
    # image_file = url_for('static', filename=f'profile_pics/' + current_user.username + '/account_img' +
    #                                         current_user.image_file)
    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)


@bp.route("/posts/<int:id>", methods=['GET', 'POST'])
@login_required
def post_detail(id):
    post_article = Post.query.get(id)
    post_article.views += 1
    db.session.commit()
    comments = Comment.query.filter_by(post_id=post_article.id).order_by(db.desc(Comment.date)).all()
    like = Like.query.filter_by(post_id=post_article.id).all()
    return render_template(
        "post_detail.html", post_article=post_article,
        comments=comments, like=like
    )


@bp.route("/create", methods=['POST', 'GET'])
@login_required
def create():
    form = PostForm()
    if form.validate_on_submit():
        try:
            language = detect(form.text.data)
        except LangDetectException:
            language = ''
        post = Post(text=form.text.data, author=current_user,
                    language=language, title=form.title.data)
        db.session.add(post)
        db.session.commit()
        flash(_('Your post is now live.'), 'success')
        return redirect(url_for('main.posts'))
    return render_template('create.html', form=form)


@bp.route("/posts/<int:id>/update", methods=['POST', 'GET'])
@login_required
def post_update(id):
    post = Post.query.get(id)
    if request.method == 'POST':
        post.title = request.form['title']
        post.text = request.form['text']
        try:
            db.session.commit()
            flash(_('Your changes have been saved.'), 'success')
            return redirect(url_for('main.post_detail', id=id))
        except:
            return _('An error occurred while updating an article.')
    else:
        return render_template("post_update.html", post=post)

# TODO: сделать функцию перевода текста при изменении текста в посте

# @bp.route("/posts/<int:id>/update", methods=['POST', 'GET'])
# @login_required
# def post_update(id):
#     post = Post.query.get(id)
#     form = PostForm()
#     if form.validate_on_submit() and request.method == 'POST':
#         post.title = request.form['title']
#         post.text = request.form['text']
#         if form.validate_on_submit():
#             try:
#                 language = detect(form.text.data)
#             except LangDetectException:
#                 language = ''
#             post = Post(text=form.text.data, author=current_user,
#                         language=language, title=form.title.data)
#             # db.session.add(post)
#             # db.session.commit(post)
#         try:
#             db.session.commit(post)
#             flash(_('Your changes have been saved.'), 'success')
#             return redirect(url_for('main.post_detail', id=id))
#         except:
#             return _('An error occurred while updating an article.')
#     else:
#         return render_template("post_update.html", post=post)


@bp.route("/posts/<int:id>/del")
@login_required
def post_delete(id):
    post = Post.query.get_or_404(id)
    try:
        db.session.delete(post)
        db.session.commit()
        flash(_('Your article was successfully deleted!'), 'info')
        return redirect(url_for('main.posts'))
    except:
        return _('An error occurred while deleting the article.')


@bp.route("/about")
def about():
    return render_template("about.html")


@bp.route('/follow/<username>', methods=['POST'])
@login_required
def follow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = db.session.scalar(
            sa.select(User).where(User.username == username))
        if user is None:
            flash(_('User %(username)s not found', username=username), 'danger')
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot follow yourself'), 'warning')
            return redirect(url_for('main.user', username=username))
        current_user.follow(user)
        db.session.commit()
        flash(_('You are following %(username)s', username=username), 'success')
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/unfollow/<username>', methods=['POST'])
@login_required
def unfollow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = db.session.scalar(
            sa.select(User).where(User.username == username))
        if user is None:
            flash(_('User %(username)s not found', username=username), 'danger')
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot unfollow yourself'), 'warning')
            return redirect(url_for('main.user', username=username), )
        current_user.unfollow(user)
        db.session.commit()
        flash(_('You are not following %(username)s.', username=username), 'success')
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    data = request.get_json()
    return {'text': translate(data['text'], data['source_language'], data['dest_language'])}


@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = db.first_or_404(sa.select(User).where(User.username == recipient))
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user, body=form.message.data)
        user.add_notification('unread_message_count', user.unread_message_count())
        db.session.add(msg)
        user.add_notification('unread_message_count', user.unread_message_count())
        db.session.commit()
        flash(_('Your message has been sent.'), category='success')
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title=_('Send Message'), form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.now(timezone.utc)
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    query = current_user.messages_received.select().order_by(
        Message.timestamp.desc())
    pagination = db.paginate(query, page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    messages = pagination.items
    return render_template('messages.html', messages=messages, pagination=pagination)


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    query = current_user.notifications.select().where(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    notifications = db.session.scalars(query)
    return [{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications]


@bp.route("/posts/<int:post_id>/comments", methods=['GET', 'POST'])
@login_required
def add_comment(post_id):
    form_add_comment = AddCommentForm()
    if request.method == 'POST' and form_add_comment.validate_on_submit():
        author = current_user
        post = Post.query.get(post_id)
        comment = Comment(author=author, comment_body=form_add_comment.comment_body.data, comment_post=post)
        db.session.add(comment)
        db.session.commit()
        flash('A comment to the post has been added.', "success")
        return redirect(url_for("main.post_detail", id=post.id))
    return render_template("comments.html", form_add_comment=form_add_comment)


@bp.route("/comment/<int:comment_id>/update", methods=['GET', 'POST'])
@login_required
def comment_update(comment_id):
    comment = Comment.query.get(comment_id)
    form = UpdateCommentForm()
    if request.method == 'GET':
        form.comment_body.data = comment.comment_body
    if request.method == 'POST' and form.validate_on_submit():
        comment.comment_body = form.comment_body.data
        try:
            db.session.commit()
            flash(_('Your changes to the comment have been saved.'), 'success')
            return redirect(url_for('main.post_detail', id=comment.post_id))
        except:
            return _('An error occurred while updating a comment.')
    else:
        return render_template("comment_update.html", comment=comment, form=form)


@bp.route("/comment/<int:comment_id>/del")
@login_required
def comment_delete(comment_id):
    comment = Comment.query.get_or_404(comment_id)
    try:
        db.session.delete(comment)
        db.session.commit()
        flash(_('Your comment was successfully deleted!'), 'danger')
        return redirect(url_for('main.post_detail', id=comment.post_id))
    except:
        return _('An error occurred while deleting the comment.')


@bp.route('/like-post/<post_id>', methods=['GET'])
@login_required
def like_post(post_id):
    post = Post.query.filter_by(id=post_id).first()
    like = Like.query.filter_by(
        user_id=current_user.id, post_id=post_id).first()

    if not post:
        flash(_('Post does not exist.'), category='error')
        # return jsonify({'error': 'Post does not exist.'}, 400)
    elif like:
        db.session.delete(like)
        db.session.commit()
    else:
        like = Like(user_id=current_user.id, post_id=post_id)
        db.session.add(like)
        db.session.commit()
    return redirect(url_for('main.post_detail', id=post.id))


@bp.route("/create-new-page")
def create_new_page():
    return render_template("create_new_page.html")



